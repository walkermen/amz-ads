<?php
/**
 * Created by PhpStorm.
 * User: dongyanan
 * Date: 2019/8/6
 * Time: 3:39 PM
 */

namespace AmazonAds\Laravel;

use Illuminate\Support\ServiceProvider;

class AmazonAdsProvider extends ServiceProvider
{
    public function boot()
    {
        // 复制自定义的文件到config目录
        if (!file_exists(config_path('amz-ads.php'))) {
            $this->publishes(array(
                __DIR__.'/config/amz-ads.php' => config_path('amz-ads.php'),
            ));
        }
    }

    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__.'/config/amz-ads.php', 'amz-ads'
        );
    }
}